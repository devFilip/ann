package hr.diplomski.ann.neuralnet;

import org.encog.ConsoleStatusReportable;
import org.encog.Encog;
import org.encog.ml.MLRegression;
import org.encog.ml.data.MLData;
import org.encog.ml.data.versatile.NormalizationHelper;
import org.encog.ml.data.versatile.VersatileMLDataSet;
import org.encog.ml.data.versatile.columns.ColumnDefinition;
import org.encog.ml.data.versatile.columns.ColumnType;
import org.encog.ml.data.versatile.sources.CSVDataSource;
import org.encog.ml.data.versatile.sources.VersatileDataSource;
import org.encog.ml.factory.MLMethodFactory;
import org.encog.ml.model.EncogModel;
import org.encog.util.csv.CSVFormat;
import org.encog.util.csv.ReadCSV;
import org.encog.util.simple.EncogUtility;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Diabetes {

  private static final List<Feature> features = new ArrayList<>(Arrays.asList(
    new Feature("race", ColumnType.nominal, 3),
    new Feature("gender", ColumnType.nominal, 4),
    new Feature("age", ColumnType.nominal, 5),
    new Feature("admission_type_id", ColumnType.nominal, 7),
    new Feature("discharge_disposition_id", ColumnType.nominal, 8),
    new Feature("admission_source_id", ColumnType.nominal, 9),
    new Feature("time_in_hospital", ColumnType.continuous, 10),
    new Feature("num_lab_procedures", ColumnType.continuous, 13),
    new Feature("num_procedures", ColumnType.continuous, 14),
    new Feature("num_medications", ColumnType.continuous, 15),
    new Feature("number_outpatient", ColumnType.continuous, 16),
    new Feature("number_emergency", ColumnType.continuous, 17),
    new Feature("number_inpatient", ColumnType.continuous, 18),
    new Feature("number_diagnoses", ColumnType.continuous, 22),
    new Feature("max_glu_serum", ColumnType.nominal, 23),
    new Feature("A1Cresult", ColumnType.nominal, 24),
    new Feature("metformin", ColumnType.nominal, 25),
    new Feature("repaglinide", ColumnType.nominal, 26),
    new Feature("nateglinide", ColumnType.nominal, 27),
    new Feature("chlorpropamide", ColumnType.nominal, 28),
    new Feature("glimepiride", ColumnType.nominal, 29),
    new Feature("acetohexamide", ColumnType.nominal, 30),
    new Feature("glipizide", ColumnType.nominal, 31),
    new Feature("glyburide", ColumnType.nominal, 32),
    new Feature("tolbutamide", ColumnType.nominal, 33),
    new Feature("pioglitazone", ColumnType.nominal, 34),
    new Feature("rosiglitazone", ColumnType.nominal, 35),
    new Feature("acarbose", ColumnType.nominal, 36),
    new Feature("miglitol", ColumnType.nominal, 37),
    new Feature("troglitazone", ColumnType.nominal, 38),
    new Feature("tolazamide", ColumnType.nominal, 39),
    new Feature("examide", ColumnType.nominal, 40),
    new Feature("citoglipton", ColumnType.nominal, 41),
    new Feature("insulin", ColumnType.nominal, 42),
    new Feature("glyburide-metformin", ColumnType.nominal, 43),
    new Feature("glipizide-metformin", ColumnType.nominal, 44),
    new Feature("glimepiride-pioglitazone", ColumnType.nominal, 45),
    new Feature("metformin-rosiglitazone", ColumnType.nominal, 46),
    new Feature("metformin-pioglitazone", ColumnType.nominal, 47),
    new Feature("change", ColumnType.nominal, 48)
  ));

  private static final List<String> featuresIgnored = Arrays.asList("medical_specialty", "payer_code", "weight", "diag_2", "diag_3");

  private static final String OUTPUT_COLUMN = "diag_1";

  VersatileMLDataSet data;

  private void run(String[] args) {
    try {
      // Load data to model
      File diabetesDataset = new File(Diabetes.class.getResource("/diabetic_small.csv").getFile());
      File testDataset = new File(Diabetes.class.getResource("/diabetic_test.csv").getFile());

      // Define the format of the data file
      VersatileDataSource source = new CSVDataSource(diabetesDataset, true,
        CSVFormat.DECIMAL_POINT);
      data = new VersatileMLDataSet(source);
      defineSourceColumns();
      defineIgnoredProperties();

      // Define output column
      ColumnDefinition outputColumn = data.defineSourceColumn(OUTPUT_COLUMN, ColumnType.nominal);

      data.analyze();
      data.defineSingleOutputOthersInput(outputColumn);

      EncogModel model = new EncogModel(data);
      model.selectMethod(data, MLMethodFactory.TYPE_FEEDFORWARD);
      model.setReport(new ConsoleStatusReportable());

      data.normalize();

      model.holdBackValidation(0.25, true, 1001);
      model.selectTrainingType(data);

      // Use a 5-fold cross-validated train.  Return the best method found.
      crossValidateAndPrintBestMethod(model, testDataset);

      // Delete data file ande shut down.
      diabetesDataset.delete();
      Encog.getInstance().shutdown();

    } catch (Exception ex) {
      ex.printStackTrace();
    }
  }

  private void defineSourceColumns() {
    for (Feature feature: features) {
      data.defineSourceColumn(feature.getName(), feature.getType());
    }
  }

  private void defineIgnoredProperties() {
    for (String feature: featuresIgnored) {
      data.defineSourceColumn(feature, ColumnType.ignore);
    }
  }

  private void crossValidateAndPrintBestMethod(EncogModel model, File testDataset) {
    MLRegression bestMethod = (MLRegression)model.crossvalidate(3, true);

    System.out.println( "Training error: " + EncogUtility.calculateRegressionError(bestMethod, model.getTrainingDataset()));
    System.out.println( "Validation error: " + EncogUtility.calculateRegressionError(bestMethod, model.getValidationDataset()));

    NormalizationHelper helper = data.getNormHelper();
    System.out.println(helper.toString());
    System.out.println("Final model: " + bestMethod);

    // Loop over the entire, original, dataset and feed it through the model
    ReadCSV csv = new ReadCSV(testDataset, true, CSVFormat.DECIMAL_POINT);
    String[] line = new String[features.size()];
    MLData input = helper.allocateInputVector();

    int counter = 0, acc_counter = 0;

    while(csv.next()) {
      counter++;
      StringBuilder result = new StringBuilder();

      for (int i = 0; i < features.size(); i++) {
        line[i] = csv.get(features.get(i).getIndex());
      }

      String correct = csv.get(19);
      helper.normalizeInputVector(line, input.getData(),false);
      MLData output = bestMethod.compute(input);
      String diagnosePrediction = helper.denormalizeOutputVectorToString(output)[0];

      if (correct.equals(diagnosePrediction)) acc_counter++;

      result.append(Arrays.toString(line));
      result.append(" -> predicted: ");
      result.append(diagnosePrediction);
      result.append("(correct: ");
      result.append(correct);
      result.append(")");

      System.out.println(result.toString());
    }

    System.out.println("Accuracy score: " + ((float)acc_counter / counter));
  }

  public static void main(String[] args) {
    Diabetes diabetes = new Diabetes();
    diabetes.run(args);
  }
}
