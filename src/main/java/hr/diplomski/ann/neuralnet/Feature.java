package hr.diplomski.ann.neuralnet;

import lombok.AllArgsConstructor;
import lombok.Getter;
import org.encog.ml.data.versatile.columns.ColumnType;

@AllArgsConstructor
public class Feature {

  @Getter
  private String name;

  @Getter
  private ColumnType type;

  @Getter
  private int index;
}
